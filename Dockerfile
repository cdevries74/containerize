FROM python:3.5-alpine3.8

LABEL maintainer="christopher.devries@libertymutual.com"

COPY * /app/

RUN pip3 install Flask
RUN apk add --no-cache curl

EXPOSE 8080

ENV DISPLAY_COLOR green \
    DISPLAY_FONT arial \
    ENVIRONMENT Prod

HEALTHCHECK --interval=30s --timeout=30s --start-period=5s --retries=3 \
 CMD curl http://localhost:8080 || exit 1

RUN chown -R 10010 /app
USER 10010

CMD python /app/app.py